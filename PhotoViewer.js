//
// Data constructs and initialization.
//

// **DO THIS**:
//   Replace BUCKET_NAME with the bucket name.
//
var albumBucketName = 'dirk-surveillance';

// **DO THIS**:
//   Replace this block of code with the sample code located at:
//   Cognito -- Manage Identity Pools -- [identity_pool_name] -- Sample Code -- JavaScript
//
// Initialize the Amazon Cognito credentials provider
AWS.config.region = 'us-west-2'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-west-2:47a9c03a-d726-4b14-8784-00baadd8429d',
});

// Create a new service object
var s3 = new AWS.S3({
  apiVersion: '2006-03-01',
  params: {Bucket: albumBucketName}
});

// A utility function to create HTML.
function getHtml(template) {
  return template.join('\n');
}


//
// Functions
//

// List the photo albums that exist in the bucket.
function listAlbums() {
  s3.listObjects({Delimiter: '/'}, function(err, data) {
    if (err) {
      return alert('There was an error listing your albums: ' + err.message);
    } else {
      var albums = data.CommonPrefixes.map(function(commonPrefix) {
        var prefix = commonPrefix.Prefix;
        var albumName = decodeURIComponent(prefix.replace('/', ''));
        return getHtml([
          '<li>',
            '<button style="margin:5px;" onclick="viewAlbum(\'' + albumName + '\')">',
              albumName,
            '</button>',
          '</li>'
        ]);
      });
      var htmlTemplate = [
        '<h2>Cameras</h2>',
        '<ul>',
          getHtml(albums),
        '</ul>',
      ]
      document.getElementById('viewer').innerHTML = getHtml(htmlTemplate);
    }
  });
}

function getVideoTimeString(videoName) {
  console.log(videoName)
  videoName = videoName.split('.')[0]
  var nameArr = videoName.split('~')
  day = nameArr[1]
  time = nameArr[2]
  dayArr = day.split('_')
  timeArr = time.split('_')
  year = dayArr[0]
  month = dayArr[1] - 1
  day = dayArr[2]
  hour = timeArr[0]
  minute = timeArr[1]
  second = timeArr[2]
  datetime = new Date(year, month, day, hour, minute, second)

  return datetime.toString()
}

// Show the photos that exist in an album.
function viewAlbum(albumName) {
  var albumPhotosKey = encodeURIComponent(albumName) + '/';
  s3.listObjects({Prefix: albumPhotosKey}, function(err, data) {
    if (err) {
      return alert('There was an error viewing your album: ' + err.message);
    }
    // 'this' references the AWS.Response instance that represents the response
    var href = this.request.httpRequest.endpoint.href;
    var bucketUrl = href + albumBucketName + '/';
    var photos = data.Contents.filter(photo => photo.Key !== albumPhotosKey).map(function(photo, i) {
      var photoKey = photo.Key;
      var photoUrl = bucketUrl + encodeURIComponent(photoKey);


      return getHtml([
        '<span>',
          '<div>',
            '<br/>',
            '<span>',
              getVideoTimeString(photoKey.replace(albumPhotosKey, '')),
            '</span>',
            '<br>',
            '<video width="320" height="240" controls>',
              '<source src="' + photoUrl + '" type="video/mp4">',
              '<source src="' + photoUrl + '" type="video/ogg">',
                'Your browser does not support the video tag.',
            '</video>',
          '</div>',
        '</span>',
      ]);
    });
    var message = photos.length ?
      '<p></p>' :
      '<p>There are no photos in this album.</p>';
    var htmlTemplate = [
      '<div>',
        '<button onclick="listAlbums()">',
          'Back To Albums',
        '</button>',
      '</div>',
      '<h2>',
        'Album: ' + albumName,
      '</h2>',
      message,
      '<div>',
        getHtml(photos),
      '</div>',
      '<h2>',
        'End of Album: ' + albumName,
      '</h2>',
      '<div>',
        '<button onclick="listAlbums()">',
          'Back To Albums',
        '</button>',
      '</div>',
    ]
    document.getElementById('viewer').innerHTML = getHtml(htmlTemplate);
    document.getElementsByTagName('img')[0].setAttribute('style', 'display:none;');
  });
}
